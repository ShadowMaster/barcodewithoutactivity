
package com.gamedeveloping.shadowmaster.barcodewithoutactivity;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.ColorInt;

import com.gamedeveloping.shadowmaster.barcodehelper.ViewfinderView;

import java.util.ArrayList;

/**
 * This view is overlaid on top of the camera preview. It adds the viewfinder rectangle and partial
 * transparency outside it, as well as the laser scanner animation and result points.
 *
 * @author dswitkin@google.com (Daniel Switkin)
 */
public class CustomViewfinderView extends ViewfinderView {

    private int scanLineY = 0;
    private @ColorInt int scanningLineColor;
    private int scanningLineSpeed = 200;
    private int fps = 60;
    private int scanLineWidth = 2;
    private long scanLineTimeLast = 0;
    private final long nanoSecInSec = 1000000000L;

    public CustomViewfinderView(Context context) {
        super(context);
    }

    public void init() {

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        maskColor = Color.parseColor("#60000000");
        resultColor = Color.parseColor("#b0000000");
        laserColor = Color.RED;
        resultPointColor = Color.GREEN;
        scannerAlpha = 0;
        possibleResultPoints = new ArrayList<>(5);
        lastPossibleResultPoints = null;

        ANIMATION_DELAY = 25L;
        scanningLineColor = Color.parseColor("#aaaaaa00");

    }


    protected void simpleCusomizing(Canvas canvas) {

        drawScanningLine(canvas);

    }

    @Override
    protected void drawLaser(Canvas canvas) {

    }

    private long getTime() {

        return System.nanoTime();

    }

    private long interpolate(float time) {

        return (long) time;

    }

    private void drawScanningLine(Canvas canvas) {

        if (scanLineTimeLast == 0) {

            scanLineTimeLast = getTime();
            scanLineY += interpolate(scanningLineSpeed / fps);

        } else {

            scanLineY += interpolate(scanningLineSpeed * (getTime() - scanLineTimeLast) / nanoSecInSec);
            scanLineTimeLast = getTime();

        }

        Rect frame = getFramingRect();

        if (scanLineY < frame.top) scanLineY = frame.top;
        if (scanLineY > frame.bottom)   scanLineY = frame.top;

        paint.setColor(scanningLineColor);

        canvas.drawRect(frame.left, scanLineY - scanLineWidth, frame.right, scanLineY + scanLineWidth, paint);

    }


//    protected void drawPreview(Canvas canvas) {
//
//        int width = canvas.getWidth();
//        int height = canvas.getHeight();
//
//        Rect frame = getFramingRect();
//
//        // Draw the exterior (i.e. outside the framing rect) darkened
//        paint.setColor(resultBitmap != null ? resultColor : maskColor);
//        canvas.drawRect(0, 0, width, frame.top, paint);
//        canvas.drawRect(0, frame.top, frame.left, frame.bottom + 1, paint);
//        canvas.drawRect(frame.right + 1, frame.top, width, frame.bottom + 1, paint);
//        canvas.drawRect(0, frame.bottom + 1, width, height, paint);
//
//    }

}
