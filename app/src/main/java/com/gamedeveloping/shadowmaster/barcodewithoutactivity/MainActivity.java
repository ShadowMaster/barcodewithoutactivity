package com.gamedeveloping.shadowmaster.barcodewithoutactivity;

import android.Manifest;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Surface;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gamedeveloping.shadowmaster.barcodehelper.BarcodeCallbackHandler;
import com.gamedeveloping.shadowmaster.barcodehelper.BarcodeConfiguration;
import com.gamedeveloping.shadowmaster.barcodehelper.BarcodeController;
import com.gamedeveloping.shadowmaster.barcodehelper.utils.SizeUtils;
import com.google.zxing.Result;

public class MainActivity extends AppCompatActivity implements BarcodeCallbackHandler{

    private RelativeLayout container;
    private BarcodeController controller;
    private Button scanButton;
    private TextView resultView;
    private final int REQUEST_CODE_ASK_PERMISSIONS = 666;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        handlePermitions();

        scanButton = (Button) findViewById(R.id.scan_button);
        resultView = (TextView) findViewById(R.id.resultView);
        container = (RelativeLayout) findViewById(R.id.container);

        final BarcodeConfiguration configuration = new BarcodeConfiguration.Builder()
                .setResizeMode(SizeUtils.ResizeMode.CENTER_OUTSIDE)
                .build();

        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                controller = new BarcodeController(MainActivity.this, container,
                        new CustomViewfinderView(MainActivity.this),
                        MainActivity.this, configuration);
                controller.init();
                controller.onResume();

            }
        });

    }

    private void handlePermitions() {

        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA},
                    REQUEST_CODE_ASK_PERMISSIONS);
            return;
        }

    }

    @Override
    public Point onPreviewSizeRequested() {
        return new Point(500,500);
    }

    @Override
    public int onSreenOrientationRequested() {
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            switch (rotation) {
                case Surface.ROTATION_0:
                case Surface.ROTATION_90:
                    return ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                default:
                    return ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
            }
        } else {
            switch (rotation) {
                case Surface.ROTATION_0:
                case Surface.ROTATION_270:
                    return ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                default:
                    return ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
            }
        }
    }

    @Override
    public void onResult(Result rawResult, Bitmap barcode, float scaleFactor) {

        resultView.setText(rawResult == null ? "Fail" : rawResult.getText());
        controller.onPause();
        controller.onResume();

    }

    @Override
    protected void onResume() {

        super.onResume();
        if (controller != null) controller.onResume();

    }

    @Override
    protected void onPause() {

        if (controller != null) controller.onPause();
        super.onPause();

    }
}
