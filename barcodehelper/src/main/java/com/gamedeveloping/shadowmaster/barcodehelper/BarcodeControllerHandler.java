
package com.gamedeveloping.shadowmaster.barcodehelper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.gamedeveloping.shadowmaster.barcodehelper.camera.CameraManager;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.Result;

import java.util.Collection;
import java.util.Map;

/**
 * This class handles all the messaging which comprises the state machine for capture.
 *
 * @author dswitkin@google.com (Daniel Switkin)
 */
final class BarcodeControllerHandler extends Handler {

  private static final String TAG = BarcodeControllerHandler.class.getSimpleName();

  private final DecodeThread decodeThread;
  private State state;
  private final CameraManager cameraManager;
  private BarcodeController controller;


  private enum State {
    PREVIEW,
    SUCCESS,
    DONE
  }

  BarcodeControllerHandler(BarcodeController controller,
                           Collection<BarcodeFormat> decodeFormats,
                           Map<DecodeHintType,?> baseHints,
                           String characterSet,
                           CameraManager cameraManager) {
    this.controller = controller;
    decodeThread = new DecodeThread(controller, decodeFormats, baseHints, characterSet,
        new ViewfinderResultPointCallback(controller.getViewFinderView()));
    decodeThread.start();
    state = State.SUCCESS;

    // Start ourselves capturing previews and decoding.
    this.cameraManager = cameraManager;
    cameraManager.startPreview();
    restartPreviewAndDecode();
  }

  @Override
  public void handleMessage(Message message) {
    switch (message.what) {
      case HandlerMessages.MESSAGE_RESTART_PREVIEW:
        restartPreviewAndDecode();
        break;
      case HandlerMessages.MESSAGE_DECODE_SUCCEDED:
        state = State.SUCCESS;
        Bundle bundle = message.getData();
        Bitmap barcode = null;
        float scaleFactor = 1.0f;
        if (bundle != null) {
          byte[] compressedBitmap = bundle.getByteArray(DecodeThread.BARCODE_BITMAP);
          if (compressedBitmap != null) {
            barcode = BitmapFactory.decodeByteArray(compressedBitmap, 0, compressedBitmap.length, null);
            // Mutable copy:
            barcode = barcode.copy(Bitmap.Config.ARGB_8888, true);
          }
          scaleFactor = bundle.getFloat(DecodeThread.BARCODE_SCALED_FACTOR);          
        }
        controller.handleDecode((Result) message.obj, barcode, scaleFactor);
        break;
      case HandlerMessages.MESSAGE_DECODE_FAILED:
        // We're decoding as fast as possible, so when one decode fails, start another.
        state = State.PREVIEW;
        cameraManager.requestPreviewFrame(decodeThread.getHandler(), HandlerMessages.MESSAGE_DECODE);
        break;
      case HandlerMessages.MESSAGE_RETURN_SCAN_RESULT:
        //context.setResult(Activity.RESULT_OK, (Intent) message.obj);
        //context.finish();
        break;
    }
  }

  public void quitSynchronously() {
    state = State.DONE;
    cameraManager.stopPreview();
    Message quit = Message.obtain(decodeThread.getHandler(), HandlerMessages.MESSAGE_QUIT);
    quit.sendToTarget();
    try {
      // Wait at most half a second; should be enough time, and onPause() will timeout quickly
      decodeThread.join(500L);
    } catch (InterruptedException e) {
      // continue
    }

    // Be absolutely sure we don't send any queued up messages
    removeMessages(HandlerMessages.MESSAGE_DECODE_SUCCEDED);
    removeMessages(HandlerMessages.MESSAGE_DECODE_FAILED);
  }

  private void restartPreviewAndDecode() {
    if (state == State.SUCCESS) {
      state = State.PREVIEW;
      cameraManager.requestPreviewFrame(decodeThread.getHandler(), HandlerMessages.MESSAGE_DECODE);
      controller.drawViewfinder();
    }
  }

}
