package com.gamedeveloping.shadowmaster.barcodehelper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.ColorInt;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.gamedeveloping.shadowmaster.barcodehelper.camera.CameraManager;
import com.gamedeveloping.shadowmaster.barcodehelper.utils.OrientationUtils;
import com.gamedeveloping.shadowmaster.barcodehelper.utils.SizeUtils;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;


public class BarcodeController implements LifeCircleCallbackHandler, SurfaceHolder.Callback{

    private final static String TAG = BarcodeController.class.toString();

    private Context context;
    private BarcodeCallbackHandler callbackHandler;
    private ViewGroup container;
    private Rect framingRect;
    private CameraManager cameraManager;
    private ViewfinderView viewfinderView;
    private BarcodeControllerHandler handler;
    private boolean hasSurface = false;
    private Collection<BarcodeFormat> decodeFormats;
    private Map<DecodeHintType,?> baseHints;
    private String characterSet;
    private Result savedResultToShow;
    private @ColorInt int resultPointsColor;
    private Rect framingRectInPreview;
    private SurfaceView surfaceView;
    private boolean wasInit = false;
    private SizeUtils.ResizeMode resizeMode = SizeUtils.ResizeMode.CENTER_OUTSIDE;
    private Point previewSize;
    private LinearLayout surfaceViewContainer;

    public BarcodeController(Context context, ViewGroup container, ViewfinderView viewfinderView,
                             BarcodeCallbackHandler callbackHandler,
                             BarcodeConfiguration barcodeConfiguration) {

        this.context = context;
        this.callbackHandler = callbackHandler;
        this.container = container;
        this.decodeFormats = barcodeConfiguration.getDecodeFormats();
        this.baseHints = barcodeConfiguration.getBaseHints();
        this.characterSet = barcodeConfiguration.getCharacterSet();
        if (barcodeConfiguration.getResizeMode() != null)
            this.resizeMode = barcodeConfiguration.getResizeMode();

        if (viewfinderView != null) {

            this.viewfinderView = viewfinderView;

        } else {

            viewfinderView = new ViewfinderView(context);

        }

        viewfinderView.setBarcodeController(this);

    }

    public BarcodeCallbackHandler getCallbackHandler() {

        return callbackHandler;

    }

    public void init() {

        surfaceView = new SurfaceView(context);
        surfaceViewContainer = new LinearLayout(context);
        surfaceViewContainer.addView(surfaceView);
        container.addView(surfaceViewContainer);
        container.addView(viewfinderView);
        resultPointsColor = Color.YELLOW;
        wasInit = true;

    }

    public void restartUI() {

        framingRect = null;
        framingRectInPreview = null;
        viewfinderView.invalidate();

    }

    ViewfinderView getViewFinderView() {

        return viewfinderView;

    }

    public Point getCameraScreenResolution() {

        if (previewSize == null) {

            return previewSize = new Point(surfaceView.getWidth(), surfaceView.getHeight());

        } else {

            return previewSize;

        }

    }

    public synchronized Rect getFramingRect() {
        if (framingRect == null) {

            Point screenResolution = getCameraScreenResolution();   //cameraManager.getScreenResolution();

            /*if (screenResolution == null) {
                // Called early, before init even finished
                return null;
            }*/

            Point scanSize = callbackHandler.onPreviewSizeRequested();
            Point containerSize = new Point(container.getWidth(), container.getHeight());

            if (scanSize == null)   scanSize = SizeUtils.getSquareRect(containerSize, 0.7f);

            Point offsets = SizeUtils.getCenterizingMargins(scanSize, containerSize);

            framingRect = new Rect(offsets.x, offsets.y, offsets.x + scanSize.x, offsets.y + scanSize.y);
        }
        return framingRect;
    }

    private void resizeSurfaceViewToCamera() {

        ViewGroup.LayoutParams params = surfaceView.getLayoutParams();
        Point cameraResolution = cameraManager.getCameraResolution();
        Point containerSize = new Point(container.getWidth(), container.getHeight());

        float necessaryAspect = OrientationUtils.findRightAspect(cameraResolution, callbackHandler.onSreenOrientationRequested());

        Point resultSize = SizeUtils.getResizingPoint(containerSize, necessaryAspect, resizeMode);
        Point resultOffset = SizeUtils.getResizingOffset(resultSize, containerSize, resizeMode);

        surfaceViewContainer.setPadding(resultOffset.x, resultOffset.y, 0, 0);
        previewSize = resultSize;
        params.width = resultSize.x;
        params.height = resultSize.y;
        surfaceView.setLayoutParams(params);

    }

    private Point getContainerSize() {

        return new Point(container.getWidth(), container.getHeight());

    }

    public synchronized Rect getFramingRectInPreview() {
        if (framingRectInPreview == null) {
            Rect framingRect = getFramingRect();
            if (framingRect == null) {
                return null;
            }
            Point cameraResolution = cameraManager.getCameraResolution();
            Point screenResolution = getCameraScreenResolution();
            if (cameraResolution == null || screenResolution == null) {
                // Called early, before init even finished
                return null;
            }

            cameraResolution = OrientationUtils.getCorrectPoint(cameraResolution, callbackHandler.onSreenOrientationRequested());
            Point correctOffset = SizeUtils.getResizingOffset(new Point(framingRect.width(), framingRect.height()), getContainerSize(), resizeMode);

            Rect rect = SizeUtils.getScaledRect(SizeUtils.getShiftedRect(
                    framingRect, SizeUtils.getScaledPoint(correctOffset, 0.5f), false), screenResolution, cameraResolution);

            framingRectInPreview = rect;

        }
        return framingRectInPreview;
    }

    PlanarYUVLuminanceSource buildLuminanceSource(byte[] data, int width, int height) {
        Rect rect = getFramingRectInPreview();
        if (rect == null) {
            return null;
        }
        // Go ahead and assume it's YUV rather than die.
        return new PlanarYUVLuminanceSource(data, width, height, rect.left, rect.top,
                rect.width(), rect.height(), false);
    }

    Handler getHandler() {

        return handler;

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

        if (wasInit) {

            cameraManager = new CameraManager(context);
            //maybe create new viewfinderview
            viewfinderView.setCameraManager(cameraManager);

            handler = null;

            //context.setRequestedOrientation(getCurrentOrientation());

            SurfaceHolder surfaceHolder = surfaceView.getHolder();
            if (hasSurface) {
                // The activity was paused but not stopped, so the surface still exists. Therefore
                // surfaceCreated() won't be called, so init the camera here.
                initCamera(surfaceHolder);
            } else {
                // Install the callback and wait for surfaceCreated() to init the camera.
                surfaceHolder.addCallback(this);
            }

        }

    }

    @Override
    public void onPause() {

        if (wasInit) {
            if (handler != null) {
                handler.quitSynchronously();
                handler = null;
            }
            cameraManager.closeDriver();
            //historyManager = null; // Keep for onActivityResult
            if (!hasSurface) {
                SurfaceHolder surfaceHolder = surfaceView.getHolder();
                surfaceHolder.removeCallback(this);
            }

        }

    }

    @Override
    public void onStop() {

    }

    private void decodeOrStoreSavedBitmap(Bitmap bitmap, Result result) {
        // Bitmap isn't used yet -- will be used soon
        if (handler == null) {
            savedResultToShow = result;
        } else {
            if (result != null) {
                savedResultToShow = result;
            }
            if (savedResultToShow != null) {
                Message message = Message.obtain(handler, HandlerMessages.MESSAGE_DECODE_SUCCEDED, savedResultToShow);
                handler.sendMessage(message);
            }
            savedResultToShow = null;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (holder == null) {
            Log.e(TAG, "*** WARNING *** surfaceCreated() gave us a null surface!");
        }
        if (!hasSurface) {
            hasSurface = true;
            initCamera(holder);
            resizeSurfaceViewToCamera();
            restartUI();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        hasSurface = false;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // do nothing
    }

    /**
     * A valid barcode has been found, so give an indication of success and show the results.
     *
     * @param rawResult The contents of the barcode.
     * @param scaleFactor amount by which thumbnail was scaled
     * @param barcode   A greyscale bitmap of the camera data which was decoded.
     */
    void handleDecode(Result rawResult, Bitmap barcode, float scaleFactor) {

        boolean fromLiveScan = barcode != null;
        if (fromLiveScan) {

            drawResultPoints(barcode, scaleFactor, rawResult);
            //viewfinderView.drawResultBitmap(barcode);
        }

        callbackHandler.onResult(rawResult, barcode, scaleFactor);

//                if (fromLiveScan) {
//                    restartPreviewAfterDelay(BULK_MODE_SCAN_DELAY_MS);
    }

    /**
     * Superimpose a line for 1D or dots for 2D to highlight the key features of the barcode.
     *
     * @param barcode   A bitmap of the captured image.
     * @param scaleFactor amount by which thumbnail was scaled
     * @param rawResult The decoded results which contains the points to draw.
     */
    private void drawResultPoints(Bitmap barcode, float scaleFactor, Result rawResult) {
        ResultPoint[] points = rawResult.getResultPoints();
        if (points != null && points.length > 0) {
            Canvas canvas = new Canvas(barcode);
            Paint paint = new Paint();
            paint.setColor(resultPointsColor);
            if (points.length == 2) {
                paint.setStrokeWidth(4.0f);
                drawLine(canvas, paint, points[0], points[1], scaleFactor);
            } else if (points.length == 4 &&
                    (rawResult.getBarcodeFormat() == BarcodeFormat.UPC_A ||
                            rawResult.getBarcodeFormat() == BarcodeFormat.EAN_13)) {
                // Hacky special case -- draw two lines, for the barcode and metadata
                drawLine(canvas, paint, points[0], points[1], scaleFactor);
                drawLine(canvas, paint, points[2], points[3], scaleFactor);
            } else {
                paint.setStrokeWidth(10.0f);
                for (ResultPoint point : points) {
                    if (point != null) {
                        canvas.drawPoint(scaleFactor * point.getX(), scaleFactor * point.getY(), paint);
                    }
                }
            }
        }
    }

    private static void drawLine(Canvas canvas, Paint paint, ResultPoint a, ResultPoint b, float scaleFactor) {
        if (a != null && b != null) {
            canvas.drawLine(scaleFactor * a.getX(),
                    scaleFactor * a.getY(),
                    scaleFactor * b.getX(),
                    scaleFactor * b.getY(),
                    paint);
        }
    }

    private void initCamera(SurfaceHolder surfaceHolder) {
        if (surfaceHolder == null) {
            throw new IllegalStateException("No SurfaceHolder provided");
        }
        if (cameraManager.isOpen()) {
            Log.w(TAG, "initCamera() while already open -- late SurfaceView callback?");
            return;
        }
        try {
            cameraManager.openDriver(surfaceHolder);
            // Creating the handler starts the preview, which can also throw a RuntimeException.
            if (handler == null) {
                handler = new BarcodeControllerHandler(this, decodeFormats, baseHints, characterSet, cameraManager);
            }
            decodeOrStoreSavedBitmap(null, null);
        } catch (IOException ioe) {
            Log.w(TAG, ioe);
        } catch (RuntimeException e) {
            // Barcode Scanner has seen crashes in the wild of this variety:
            // java.?lang.?RuntimeException: Fail to connect to camera service
            Log.w(TAG, "Unexpected error initializing camera", e);
        }
    }

    void restartPreviewAfterDelay(long delayMS) {
        if (handler != null) {
            handler.sendEmptyMessageDelayed(HandlerMessages.MESSAGE_RESTART_PREVIEW, delayMS);
        }
    }

    void drawViewfinder() {
        viewfinderView.drawViewfinder();
    }

}
