package com.gamedeveloping.shadowmaster.barcodehelper.utils;

import android.graphics.Point;
import android.graphics.Rect;

/**
 * Created by ShadowMaster on 25.07.2017.
 */

public class SizeUtils {

    public enum ResizeMode {

        CENTER_INSIDE,
        CENTER_OUTSIDE,
        LEFT_TOP_INSIDE,
        LEFT_TOP_OUTSIDE

    }

    public static Point getScaledPoint(Point point, float scale) {

        return new Point((int) (point.x * scale), (int) (point.y * scale));

    }

    public static Rect getShiftedRect(Rect rect, Point shift, boolean direction) {

        int sign = 1;
        if (!direction)  sign = -1;

        return new Rect(
                rect.left + shift.x * sign,
                rect.top + shift.y * sign,
                rect.right + shift.x * sign,
                rect.bottom + shift.y * sign
        );

    }

    public static Rect getScaledRect(Rect fromRect, Point from, Point to) {

        return new Rect(
                fromRect.left * to.x / from.x,
                fromRect.top * to.x / from.x,
                fromRect.right * to.y / from.y,
                fromRect.bottom * to.y / from.y
                );

    }

    public static Point getCenterizingMargins(Point innerRect, Point container) {

        return new Point((container.x - innerRect.x) / 2, (container.y - innerRect.y) / 2);

    }

    public static Point getSquareRect(Point container, float scaleFactor) {

        int bestSize = (int) (Math.min(container.x, container.y) * scaleFactor);

        return new Point(bestSize, bestSize);

    }

    public static Point getResizingPoint(Point containerSize, float necessaryAspect, ResizeMode mode) {

        int tempWidth = (int) (necessaryAspect * containerSize.y);
        boolean isHorizontalExceedFactor;

        switch (mode) {

            case LEFT_TOP_INSIDE:
            case CENTER_INSIDE:

                isHorizontalExceedFactor = tempWidth > containerSize.x;

                break;

            case LEFT_TOP_OUTSIDE:
            case CENTER_OUTSIDE:

                isHorizontalExceedFactor = tempWidth < containerSize.x;

                break;

            default:

                return new Point(containerSize.x, containerSize.y);

        }

        if (isHorizontalExceedFactor) {

            return new Point(containerSize.x, (int) (containerSize.x / necessaryAspect));

        } else {

            return new Point(tempWidth, containerSize.y);

        }
    }

    public static Point getResizingOffset(Point innerSize, Point containerSize, ResizeMode mode) {

        switch (mode) {

            case CENTER_INSIDE:

                return getCenterizingMargins(innerSize, containerSize);

            case LEFT_TOP_INSIDE:
            case LEFT_TOP_OUTSIDE:
            default:
                return new Point(0, 0);

        }

    }

}
