package com.gamedeveloping.shadowmaster.barcodehelper.utils;

import android.content.pm.ActivityInfo;
import android.graphics.Point;

/**
 * Created by ShadowMaster on 25.07.2017.
 */

public class OrientationUtils {

    public static float findRightAspect(Point size, int orientation) {

        switch (orientation) {

            case ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE:
            case ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE:
                return (float) size.x / size.y;
            case ActivityInfo.SCREEN_ORIENTATION_PORTRAIT:
            case ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT:
                return (float) size.y / size.x;
            default:
                return (float) size.x / size.y;
        }

    }

    public static Point getCorrectPoint(Point size, int orientation) {

        switch (orientation) {

            case ActivityInfo.SCREEN_ORIENTATION_PORTRAIT:
            case ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT:

                return new Point(size.y, size.x);

            case ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE:
            case ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE:
            default:

                return size;

        }

    }

}
