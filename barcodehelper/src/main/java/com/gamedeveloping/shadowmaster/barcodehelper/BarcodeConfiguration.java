package com.gamedeveloping.shadowmaster.barcodehelper;

import com.gamedeveloping.shadowmaster.barcodehelper.utils.SizeUtils;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;

import java.util.Collection;
import java.util.Map;

/**
 * Created by ShadowMaster on 25.07.2017.
 */

public class BarcodeConfiguration {

    private final Collection<BarcodeFormat> decodeFormats;
    private final Map<DecodeHintType,?> baseHints;
    private final String characterSet;
    private final SizeUtils.ResizeMode resizeMode;

    private BarcodeConfiguration(Builder builder) {

        decodeFormats = builder.decodeFormats;
        baseHints = builder.baseHints;
        characterSet = builder.characterSet;
        resizeMode = builder.resizeMode;

    }

    public Collection<BarcodeFormat> getDecodeFormats() {
        return decodeFormats;
    }

    public Map<DecodeHintType, ?> getBaseHints() {
        return baseHints;
    }

    public String getCharacterSet() {
        return characterSet;
    }

    public SizeUtils.ResizeMode getResizeMode() {
        return resizeMode;
    }

    public static class Builder {

        Collection<BarcodeFormat> decodeFormats;
        Map<DecodeHintType,?> baseHints;
        String characterSet;
        SizeUtils.ResizeMode resizeMode;

        public Builder setDecodeFormats(Collection<BarcodeFormat> decodeFormats) {

            this.decodeFormats = decodeFormats;
            return this;

        }

        public Builder setBaseHints(Map<DecodeHintType, ?> baseHints) {

            this.baseHints = baseHints;
            return this;

        }

        public Builder setCharacterSet(String characterSet) {

            this.characterSet = characterSet;
            return this;

        }

        public Builder setResizeMode(SizeUtils.ResizeMode resizeMode) {

            this.resizeMode = resizeMode;
            return this;

        }

        public BarcodeConfiguration build() {

            return new BarcodeConfiguration(this);

        }

    }


}
