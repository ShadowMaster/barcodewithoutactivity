package com.gamedeveloping.shadowmaster.barcodehelper;

/**
 * Created by ShadowMaster on 24.07.2017.
 */

class HandlerMessages {

    final static int MESSAGE_DECODE = 0;
    final static int MESSAGE_QUIT = 1;
    final static int MESSAGE_DECODE_FAILED = 2;
    final static int MESSAGE_DECODE_SUCCEDED = 3;
    final static int MESSAGE_RESTART_PREVIEW = 4;
    final static int MESSAGE_RETURN_SCAN_RESULT = 5;

}
