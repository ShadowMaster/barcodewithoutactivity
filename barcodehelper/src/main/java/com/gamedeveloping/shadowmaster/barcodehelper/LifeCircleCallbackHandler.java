package com.gamedeveloping.shadowmaster.barcodehelper;

/**
 * Created by ShadowMaster on 18.07.2017.
 */

interface LifeCircleCallbackHandler {

    void onStart();
    void onResume();
    void onPause();
    void onStop();

}
