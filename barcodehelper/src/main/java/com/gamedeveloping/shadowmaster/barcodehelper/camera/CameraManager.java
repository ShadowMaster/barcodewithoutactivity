package com.gamedeveloping.shadowmaster.barcodehelper.camera;

import android.content.Context;
import android.graphics.Point;
import android.hardware.Camera;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;

import com.gamedeveloping.shadowmaster.barcodehelper.camera.open.OpenCamera;
import com.gamedeveloping.shadowmaster.barcodehelper.camera.open.OpenCameraInterface;

import java.io.IOException;

/**
 * Created by ShadowMaster on 18.07.2017.
 */

public class CameraManager {

    private static final String TAG = CameraManager.class.getSimpleName();

    private final Context context;
    private final CameraConfigurationManager configManager;
    private OpenCamera camera;
    private AutoFocusManager autoFocusManager;
    private boolean previewing;
    private int requestedCameraId = -1;

        private final PreviewCallback previewCallback;

        public CameraManager(Context context) {
            this.context = context;
            this.configManager = new CameraConfigurationManager(context);
            previewCallback = new PreviewCallback(configManager);
        }

        public CameraConfigurationManager getConfigManager() {

            return configManager;

        }

        public Point getScreenResolution() {

            return configManager.getScreenResolution();

        }

        public Point getCameraResolution() {

            return configManager.getCameraResolution();

        }

        public synchronized void openDriver(SurfaceHolder holder) throws IOException {
            OpenCamera theCamera = camera;

            if (theCamera == null) {
                theCamera = OpenCameraInterface.open(requestedCameraId);
                if (theCamera == null) {
                    throw new IOException("Camera.open() failed to return object from driver");
                }
                camera = theCamera;
            }

            configManager.initFromCameraParameters(theCamera);

            Camera cameraObject = theCamera.getCamera();
            Camera.Parameters parameters = cameraObject.getParameters();
            String parametersFlattened = parameters == null ? null : parameters.flatten(); // Save these, temporarily
            try {
                configManager.setDesiredCameraParameters(theCamera, false);
            } catch (RuntimeException re) {
                // Driver failed
                Log.w(TAG, "Camera rejected parameters. Setting only minimal safe-mode parameters");
                Log.i(TAG, "Resetting to saved camera params: " + parametersFlattened);
                // Reset:
                if (parametersFlattened != null) {
                    parameters = cameraObject.getParameters();
                    parameters.unflatten(parametersFlattened);
                    try {
                        cameraObject.setParameters(parameters);
                        configManager.setDesiredCameraParameters(theCamera, true);
                    } catch (RuntimeException re2) {
                        // Well, darn. Give up
                        Log.w(TAG, "Camera rejected even safe-mode parameters! No configuration");
                    }
                }
            }
            cameraObject.setPreviewDisplay(holder);

        }

        public synchronized boolean isOpen() {
            return camera != null;
        }

        public synchronized void closeDriver() {
            if (camera != null) {
                camera.getCamera().release();
                camera = null;
            }
        }

        public synchronized void startPreview() {
            OpenCamera theCamera = camera;
            if (theCamera != null && !previewing) {
                theCamera.getCamera().startPreview();
                previewing = true;
                autoFocusManager = new AutoFocusManager(context, theCamera.getCamera());
            }
        }

        public synchronized void stopPreview() {
            if (autoFocusManager != null) {
                autoFocusManager.stop();
                autoFocusManager = null;
            }
            if (camera != null && previewing) {
                camera.getCamera().stopPreview();
                previewCallback.setHandler(null, 0);
                previewing = false;
            }
        }


        public synchronized void requestPreviewFrame(Handler handler, int message) {
            OpenCamera theCamera = camera;
            if (theCamera != null && previewing) {
                previewCallback.setHandler(handler, message);
                theCamera.getCamera().setOneShotPreviewCallback(previewCallback);
            }
        }

}
