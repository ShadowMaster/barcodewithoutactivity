package com.gamedeveloping.shadowmaster.barcodehelper;

import android.graphics.Bitmap;
import android.graphics.Point;

import com.google.zxing.Result;

/**
 * Created by ShadowMaster on 17.07.2017.
 */

public interface BarcodeCallbackHandler {

    /**
     * Called when library need to get scan rectangle.
     */
    Point onPreviewSizeRequested();
    /**
     * Called when library need to get screen orientation.
     */
    int onSreenOrientationRequested();
    /**
     * Getting results.
     */
    void onResult(Result rawResult, Bitmap barcode, float scaleFactor);

}
